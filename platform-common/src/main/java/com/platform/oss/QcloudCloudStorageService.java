package com.platform.oss;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.Date;
import java.util.concurrent.*;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.apache.http.entity.ContentType;
import org.springframework.web.multipart.MultipartFile;

import com.platform.utils.RRException;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.exception.CosClientException;
import com.qcloud.cos.exception.CosServiceException;
import com.qcloud.cos.model.ObjectMetadata;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.UploadResult;
import com.qcloud.cos.region.Region;
import com.qcloud.cos.transfer.TransferManager;
import com.qcloud.cos.transfer.Upload;

import javax.servlet.http.HttpServletRequest;

/**
 * 腾讯云存储
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-03-26 20:51
 */
public class QcloudCloudStorageService extends CloudStorageService {
	private COSClient client;

	public QcloudCloudStorageService(CloudStorageConfig config) {
		this.config = config;

		// 初始化
		init();
	}

	private void init() {
		// 1 初始化用户身份信息(appid, secretId, secretKey)
		COSCredentials credentials = new BasicCOSCredentials(config.getQcloudSecretId(), config.getQcloudSecretKey());
		// 2 设置bucket的区域, COS地域的简称请参照 https://www.qcloud.com/document/product/436/6224
		// 初始化客户端配置 设置bucket所在的区域，华南：gz 华北：tj 华东：sh
		ClientConfig clientConfig = new ClientConfig(new Region(config.getQcloudRegion()));
		// 3 生成cos客户端
		client = new COSClient(credentials, clientConfig);

		/*// 3 生成cos客户端
		COSClient cosclient = new COSClient(credentials, clientConfig);
		// bucket名称, 需包含appid
		String bucketName = "publicreadbucket-1251668577";
		// 删除bucket
		cosclient.deleteBucket(bucketName);

		// 关闭客户端
		cosclient.shutdown();*/
	}

	@Override
	public String upload(MultipartFile file) throws Exception {

		String fileName = file.getOriginalFilename();
		
		String prefix = fileName.substring(fileName.lastIndexOf(".") + 1);
		System.out.println(fileName);
		System.out.println(prefix);
		return upload(file.getInputStream(), getPath(config.getQcloudPrefix()) + "." + prefix);
	}

	@Override
	public String upload(InputStream inputStream, String path) {
		// 腾讯云必需要以"/"开头
		if (!path.startsWith("/")) {
			path = "/" + path;
		}

		int size = 32;
//		ExecutorService threadPool = new ThreadPoolExecutor(size,size,0L, TimeUnit.MILLISECONDS,new LinkedBlockingQueue<Runnable>());

		ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("thread-call-runner-%d").build();
//		int size = services.size();
		ExecutorService threadPool = new ThreadPoolExecutor(size,size,0L,TimeUnit.MILLISECONDS,new LinkedBlockingQueue<Runnable>(),namedThreadFactory);


//		ExecutorService threadPool = Executors.newFixedThreadPool(32);
		// 传入一个 threadpool, 若不传入线程池, 默认 TransferManager 中会生成一个单线程的线程池。
		TransferManager transferManager = new TransferManager(client, threadPool);
		// .....(提交上传下载请求, 如下文所属)
		// 创建上传Object的Metadata
		ObjectMetadata meta = new ObjectMetadata();

		meta.setContentType(ContentType.APPLICATION_OCTET_STREAM.getMimeType());
		
		try {
			// 这里有个风险，因为available返回的是int类型，有长度限制，如果文件大，这个不适用。
			meta.setContentLength(inputStream.available());
		} catch (IOException e1) {
			throw new RRException("文件流错误，" + e1.getMessage());
		
		}

		PutObjectRequest putObjectRequest = new PutObjectRequest(config.getQcloudBucketName(),
				 path, inputStream, meta);
		// 本地文件上传
		Upload upload = transferManager.upload(putObjectRequest);
		// 等待传输结束（如果想同步的等待上传结束，则调用 waitForCompletion）

		try {
			UploadResult uploadResult = upload.waitForUploadResult();

		} catch (CosServiceException e) {

			throw new RRException("服务异常，" + e.getErrorMessage());

		} catch (CosClientException e) {

			throw new RRException("客户端异常，" + e.getMessage());
		} catch (InterruptedException e) {

			throw new RRException("系统异常，" + e.getMessage());
		} finally {
			// 关闭 TransferManger
			transferManager.shutdownNow();
		}
		// 例如：https://paddy-1256559626.cosbj.myqcloud.com/images/demo/20180426/0034397501f917.png
		return config.getQcloudDomain() + path;
	}

	//改写上传文件地方
	public URL picCOS(File cosFile,String path) throws Exception {
		// 腾讯云必需要以"/"开头
		if (!path.startsWith("/")) {
			path = "/" + path;
		}
		COSCredentials cred = new BasicCOSCredentials(config.getQcloudSecretId(),config.getQcloudSecretKey());
		// 2 设置bucket的区域, COS地域的简称请参照
		// https://cloud.tencent.com/document/product/436/6224
		ClientConfig clientConfig = new ClientConfig(new Region("ap-guangzhou"));
		// 3 生成cos客户端
		COSClient cosClient = new COSClient(cred, clientConfig);
		String bucketName = config.getQcloudBucketName();
		String key = "test/"+System.currentTimeMillis() + ".png";

		// 简单文件上传, 最大支持 5 GB, 适用于小文件上传, 建议 20 M 以下的文件使用该接口
		// 大文件上传请参照 API 文档高级 API 上传
		// 指定要上传到 COS 上的路径

		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, cosFile);
		cosClient.putObject(putObjectRequest);
		cosClient.shutdown();
		Date expiration = new Date(System.currentTimeMillis() + 5 * 60 * 10000);
		URL url = cosClient.generatePresignedUrl(bucketName, key, expiration);
		return url;
	}


	@Override
	public String upload(byte[] data, String path) {
		// 这个方法在腾讯新版sdk中已经弃用
		return null;
	}
	
	public void delete(String path) {
		client.deleteObject(config.getQcloudBucketName(), path);
	}



}
